# Multipurpose PIC16F887 Circuit: Voltmeter, Clock, and Safety Mechanism

This repository contains the code and schematics for a versatile electronic circuit built for the Digital Electronics course.

## Overview

Experience a comprehensive project utilizing the PIC 16F887 microcontroller, integrating interrupts, timers, a matrix keypad, analog-to-digital converter (ADC), and serial communication. The project features a voltmeter, clock, and a safety mechanism, showcasing a culmination of digital electronics principles.

## Schematic
![Circuit Diagram](img/circuit.png)

### Features

- **Clock Display:** Set and display time in 24-hour format using the matrix keypad.
- **Voltage and Current Readings:** Real-time display of voltage (0-5V) and current (0-5A) on four 7-segment displays.
- **Operational Modes:** Four modes displayed via Port E in binary code.
- **Multiplexing:** Utilizes a single 7-bit data bus for display across all four segments.
- **Serial Communication:** Transmit voltage/current values to a PC for data accessibility.

### Safety Mechanism

An anti-short circuit mechanism safeguards user circuits by activating a relay when voltage approaches zero, disconnecting the power source to prevent damage.

## Technical Details

This project employs a variety of components, including PIC16F887, potentiometers, transistors, 7-segment displays, resistors, capacitors, LEDs, a relay, matrix keypad, and connectors. The README file here contains detailed technical information, including calculations, component utilization, polarization specifics, baud rate, ADC sampling time calculations, and a reflective conclusion on the project's success.

## Getting Started

To clone this repository and explore the project:

```bash
git clone https://gitlab.com/english6060242/embedded-systems/pic/asm/01-voltimeter_and_clock.git
```

## Usage
Ready-made Hex File: Utilize the provided .hex file in the bin folder.

or

Compile the Code: Compile it yourself using MPLAB or similar software. Once compiled, add the generated hex file to your simulation or use it to program your PIC if the circuit is constructed physically.

Note: In the case of physical implementation, ensure to integrate a diode in the relay, as illustrated in the circuit image. This diode is crucial to prevent potential damage caused by the inductor.

## License
This project is licensed under the [MIT License](LICENSE.md).