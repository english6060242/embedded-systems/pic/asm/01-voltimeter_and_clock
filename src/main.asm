#include "p16f887.inc"

; CONFIG1
; __config 0xFFE1
 __CONFIG _CONFIG1, _FOSC_INTRC_NOCLKOUT & _WDTE_OFF & _PWRTE_ON & _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _IESO_OFF & _FCMEN_OFF & _LVP_OFF
; CONFIG2
; __config 0xFFFF
 __CONFIG _CONFIG2, _BOR4V_BOR40V & _WRT_OFF

	ORG	0X00
	GOTO	START
 
	ORG	0X04
	GOTO	INTER
			;   Declaration of variables
CBLOCK	0X20
	MODO		;   Register containing the operating mode. Different data will be displayed on the screen according to this.
	UNIDAD		;   The value read by the ADC will be divided into El valor leido por el ADC  units, tenths, and hundredths
	DECENA		;   in order to visualize such value in the displays.
	CENTENA		;   This digits will be saved into registers for easy manipulation.
	SELDISP		;   This register contains the value to enable/disable displays as nedded.
	GW		    ;   Context saving register for the W register.
	GSTAT		;   Context saving register for the STATUS register.
	AUX		    ;   Auxiliar variable 
	AUX2		;   Auxiliar variable 
	AUX3		;   Auxiliar variable 
	CONT		;   This counter is used to send logial HIGH values to the rows of the matrix keyboard.
	CONT2		;   This counter's purposes is incrementing CONT every three TMR0 interrupts.
	CONT3		;   This counter's purpose is to generate a second, using TMR0 5ms (which is why it will be initializen in 200).
	CMOD		;   This counter represents the system's mode.
	MCONT		;   Counter for selecting which character to send via the Serial port.
	ECONT		;   Counter for choosing which character to send via the Serial port.
	CONTCS		;   Counter for sending via the Serial port every 3 timer0 interruptions.
	NUM1		
	NUM2
	NUM3		;   These are the numbers to be sent to the Display, containing the decimal value of the ADC reading.
	NUMD		;   Value to display at a given moment by PORTD (Data for the Displays).
	SEG		    ;   Seconds, when they reach 60, increase by 1 minute.
	UMIN		;   Unit of the Minutes.
	DMIN		;   Tens of the Minutes
	UHORA		;   Unit of the Hours.
	DHORA		;   Tens of the Hours.
	SUMIN		;   Register to set Unit of the Minutes.
	SDMIN		;   Register to set Tens of the Minutes.
	SUHORA		;   Register to set Unit of the Hours.
	SDHORA		;   Register to set Tens of the Hours.
	CHARU		;   Character to send the unit of the value read by the ADC.
	CHARD		;   Character to send the tenth of the value read by the ADC.
	CHARC		;   Character to send the hundredth of the value read by the ADC.
	CHAR0		;   Character to send via the serial port
	ENDC		;	End of Variables declaration.

START	CLRF    NUMD	    ;Variable initialization 
	CLRF	SUMIN
	CLRF	SDMIN
	CLRF	SUHORA
	CLRF	SELDISP
	CLRF	CONT
	CLRF	GW
	CLRF	GSTAT
	CLRF	CONT2
	CLRF	MODO
	CLRF	AUX
	CLRF	CONT3
	CLRF	UMIN
	CLRF	DMIN
	MOVWF	UHORA
	MOVWF	DHORA
	MOVLW	b'10100000'	;   Enabled Interrupts: Timer 0.
	MOVWF	INTCON
	BANKSEL	ADCON0		;   ADC's Configuration: Conversion Clock: Fosc/32 ; 
	MOVLW	b'11000101'	;   Channel 1; ADON=1 -> Conversion Enabled.
	MOVWF	ADCON0		;
	BSF	STATUS,RP0
	MOVLW	b'11000110'	;   OPTION_REG Config: Pull up resistors disabled.
	MOVWF	OPTION_REG	;   Prescaler = 128, assigned to TMR0; TMR0 Clock Source = Fosc/4.
	MOVLW	b'01110000'
	MOVWF	TRISB		;    Input/Output Configuration
	MOVLW	0X03
	MOVWF	TRISA
	CLRF	TRISE
	CLRF	TRISC
	BSF	TRISC,7
	CLRF	TRISD
	BANKSEL	ADCON1		;   ADC Config
	MOVLW	b'00001110'	;   Voltage Reference with VCC (V+) and GND (V-)
	MOVWF	ADCON1		;   Result Format: Left Justified
	BSF	STATUS,RP1
	MOVLW	b'00000011'
	MOVWF	ANSEL		;   RA0 and RA1 are analog inputs. The rest are digital inputs/outputs
	BCF	STATUS,RP0
	BCF	STATUS,RP1
	BANKSEL	ANSELH
	CLRF	ANSELH
	BANKSEL	TMR0
	MOVLW	d'217'		;   TMR0 Load = 217. Overflow every 5ms.
	MOVWF	TMR0
	MOVLW	0X01
	MOVWF	SELDISP		;   Selecting the first Display
	MOVWF	PORTC		;   Display on PORTC
	CLRF	PORTC		;   Clear PORTC
	CLRF	PORTE		;   Clear PORTE
	MOVLW	0X00		;   Load 0 in the Unit and Tens of Minutes and Hours setters
	MOVWF	SUMIN
	MOVWF	SDMIN
	MOVWF	SUHORA
	MOVWF	SDHORA
	MOVLW	d'3'	   
	MOVWF	CONT2
	MOVLW	0X04
	MOVWF	CONT
	MOVLW	0X04
	MOVWF	MODO
	MOVLW	d'200'
	MOVWF	CONT3
	CLRF	CMOD
	BCF	PORTA,5
	CLRF	CHAR0
	MOVLW	0X01
	MOVWF	ECONT
    MOVLW   d'3'        ; Send via serial port every
    MOVWF   CONTCS      ;

MAIN    CALL    BARRIDO     ; Call sweep and push for the matrix keypad
    CALL    PUSHED
    CALL    ADC         ; Call the ADC
    MOVF    CMOD, W     ; Move CMOD to W
    MOVWF   PORTE       ; Load PORTE with W
    BCF     STATUS, C   ; Clear the carry
    MOVF    AUX2, W     ; Load W with AUX2 (Voltage value)
    SUBLW   d'60'       ; Subtract 60 from AUX2
    BTFSS   STATUS, C   ; Check if the carry is 0
    GOTO    CORTAR      ; If so, go to cut
    GOTO    NOCORT      ; If not, go to not cut
    GOTO    MAIN        ; Return to MAIN

CORTAR  BCF     PORTA, 5    ; Set the 5th bit of PORTA to 1
    GOTO    MAIN        ; Return to MAIN

NOCORT  BSF     PORTA, 5    ; Set the 5th bit of PORTA to 0
    GOTO    MAIN        ; Return to MAIN

TMODO   BTFSC   MODO, 0     ; Function to choose the mode
    GOTO    SETHORA     ; Mode 0, Set Time
    BTFSC   MODO, 1
    GOTO    VERHORA     ; Mode 1, Display Time
    BTFSC   MODO, 2
    GOTO    SELDIGI     ; Mode 2, ADC Voltage
    BTFSC   MODO, 3
    GOTO    SELDIGI     ; Mode 3, ADC Current

BARRIDO MOVF    CONT, W     ; Move CONT to W
    CALL    TFILAS      ; Call the TFILAS table
    MOVWF   PORTB       ; Display what TFILAS returns on PORTB
    RETURN              ; Return
	
TFILAS	ADDWF	PCL,F	    
	RETLW	0X00	    ;	Returns 0
	RETLW	0X01	    ;	Returns 1
	RETLW	0X02	    ;	Returns 2
	RETLW	0X04	    ;	Returns 4
	RETLW	0X08	    ;	Returns 8
	
SETHORA BTFSC   SELDISP, 0  ; According to the enabled Display, we will set the hour
    GOTO    SDUMIN      ; Go to set the unit of minutes
    BTFSC   SELDISP, 1
    GOTO    SDDMIN      ; Go to set the ten of minutes
    BTFSC   SELDISP, 2
    GOTO    SDUHORA     ; Go to set the unit of hours
    BTFSC   SELDISP, 3
    GOTO    SDDHORA     ; Go to set the ten of hours
    
SDUMIN  MOVF    SUMIN, W    ; Move the unit of minutes setter to W  
    MOVWF   NUMD        ; Load W with NUMD
    GOTO    MOSTRAR     ; Call show, with NUMD

SDDMIN  MOVF    SDMIN, W    ; Move the ten of minutes setter to W 
    MOVWF   NUMD        ; Load W with NUMD
    GOTO    MOSTRAR     ; Call show, with NUMD

SDUHORA MOVF    SUHORA, W   ; Move the unit of hours setter to W 
    MOVWF   NUMD        ; Load W with NUMD
    GOTO    MOSTRAR     ; Call show, with NUMD

SDDHORA MOVF    SDHORA, W   ; Move the ten of hours setter to W 
    MOVWF   NUMD        ; Load W with NUMD
    GOTO    MOSTRAR     ; Call show, with NUMD

VERHORA BTFSC   SELDISP, 0  ; Function to view the hour according to the enabled Display
    GOTO    DUMIN       ; If it's the first display, call DUMIN
    BTFSC   SELDISP, 1
    GOTO    DDMIN       ; If it's the second display, call DDMIN
    BTFSC   SELDISP, 2
    GOTO    DUHORA      ; If it's the third display, call DUHORA
    BTFSC   SELDISP, 3
    GOTO    DDHORA      ; If it's the fourth display, call DDHORA 
	
DUMIN   MOVF    UMIN, W     ; Move UMIN to W  
        MOVWF   NUMD        ; Load W with NUMD
        GOTO    MOSTRAR     ; Go to SHOW

DDMIN   MOVF    DMIN, W     ; Move DMIN to W
        MOVWF   NUMD        ; Load W with NUMD
        GOTO    MOSTRAR     ; Go to SHOW

DUHORA  MOVF    UHORA, W    ; Move UHORA to W
        MOVWF   NUMD        ; Load W with NUMD
        GOTO    MOSTRAR     ; Go to SHOW

DDHORA  MOVF    DHORA, W    ; Move DHORA to W
        MOVWF   NUMD        ; Load W with NUMD
        GOTO    MOSTRAR     ; Go to SHOW

; KEYBOARD

TC1     DECF    CONT, W     ; Decrement CONT and store it in W
        CALL    TTC1        ; Call TTC1
BC1     BTFSC   PORTB, 4    ; Check the 4th bit of PORTB 
        GOTO    BC1         ; If it's 1, go to BC1
        MOVWF   AUX         ; If it's 0, load W with AUX
        CALL    MODIFY      ; Call MODIFY
        RETURN

TTC1    ADDWF   PCL, F      ; Table
        RETLW   D'1'        ; Return 1
        RETLW   D'4'        ; Return 4
        RETLW   D'7'        ; Return 7
        RETLW   0X0A        ; Return a line feed

TC2     DECF    CONT, W     ; Decrement CONT and store it in W
        CALL    TTC2        ; Call TTC2
BC2     BTFSC   PORTB, 5    ; Check the 5th bit of PORTB 
        GOTO    BC2         ; If it's 1, go to BC2
        MOVWF   AUX         ; If it's 0, load W with AUX
        CALL    MODIFY      ; Call MODIFY
        RETURN
	
TTC2    ADDWF   PCL, f      ; Table
        RETLW   D'2'        ; Returns 2
        RETLW   D'5'        ; Returns 5
        RETLW   D'8'        ; Returns 8
        RETLW   D'0'        ; Returns 0

TC3     DECF    CONT, W     ; Decrement CONT and store it in W
        CALL    TTC3        ; Call TTC3
BC3     BTFSC   PORTB, 6    ; Check the 6th bit of PORTB 
        GOTO    BC3         ; If it's 1, go to BC3
        MOVWF   AUX         ; If it's 0, load W with AUX
        CALL    MODIFY      ; Call MODIFY
        RETURN

TTC3    ADDWF   PCL, f      ; Table
        RETLW   D'3'        ; Returns 3
        RETLW   D'6'        ; Returns 6
        RETLW   D'9'        ; Returns 9
        RETLW   0X0B        ; Returns a line feed

; To display ADC

SELDIGI BTFSC   SELDISP, 0  ; Function to select the digit used
        GOTO    TUNI        ; First
        BTFSC   SELDISP, 1
        GOTO    TDEC        ; Second
        BTFSC   SELDISP, 2
        GOTO    TCEN        ; Third
        BTFSC   SELDISP, 3
        GOTO    TE          ; Fourth
	
TUNI    MOVF    NUM1, W     ; If it's the first digit, move NUM1 to W
        MOVWF   NUMD        ; Load NUMD with W
        GOTO    MOSTRAR     ; Go to show

TDEC    MOVF    NUM2, W     ; If it's the second digit, move NUM2 to W
        MOVWF   NUMD        ; Load NUMD with W
        GOTO    MOSTRAR     ; Go to show

TCEN    MOVF    NUM3, W     ; If it's the third digit, move NUM3 to W
        MOVWF   NUMD        ; Load NUMD with W
        GOTO    MOSTRAR     ; Go to show

TE      MOVLW   0X0E        ; If it's the fourth digit, move "E" to W
        MOVWF   NUMD        ; Load NUMD with W
        GOTO    MOSTRAR     ; Go to show

DISPLAY ADDWF   PCL         ; Table for common cathode 7-segment display a=lsb
.0	RETLW	0X3F	    ;	b'00111111';0
.1	RETLW	0X06	    ;	b'00000110';1
.2	RETLW	0X5B	    ;	b'01011011';2
.3	RETLW	0X4F	    ;	b'01001111';3
.4	RETLW	0X66	    ;	b'01100110';4
.5	RETLW	0X6D	    ;	b'01101101';5
.6	RETLW	0X7D	    ;	b'01111101';6
.8	RETLW	0X07	    ;	b'00000111';7
.7	RETLW	0X7F	    ;	b'01111111';8
.9	RETLW	0X67	    ;	b'01100111';9
.A	RETLW	0X00
.B	RETLW	0X00
.C	RETLW	b'00111001'
.D	RETLW	0X00
.E	RETLW	b'01111001'
.F	RETLW	0X00
	
INTER   MOVWF   GW          ; Interrupt routine
        SWAPF   STATUS,W    ; Save W and STATUS registers
        MOVWF   GSTAT
        BTFSC   INTCON,2    ; Configure INTCON register for timer
        GOTO    TIMMER      ; Go to timer
VOLVER  MOVLW   b'10100000' ; Configure INTCON register
        MOVWF   INTCON
        SWAPF   GSTAT,W     ; Retrieve W and STATUS registers
        MOVWF   STATUS
        MOVF    GW,W
        RETFIE

TIMMER  DECFSZ  CONTCS,F    ; Decrement CONTCS and save in itself, when it reaches 0
        GOTO    NOENV       ; If it's 1, go to NOENV
        CALL    ENVIAR      ; It reached 0, go to ENVIAR
NOENV   BCF     INTCON,2    ; Configure INTCON register
        BTFSC   MODO,1      ; Check if it's in mode 1
        DECFSZ  CONT3       ; If it is, decrement CONT3 until it's zero and jump to call UNSEG
        GOTO    SEGUIR      ; If not, go to SEGUIR
        CALL    UNSEG       ; Call UNSEG
SEGUIR  BCF     STATUS,C    ; Set carry to 0
        RLF     SELDISP,F   ; Rotate SELDISP to move through displays
        BTFSC   SELDISP,4   ; Check if it has passed through all displays
        CALL    RESETSD     ; If yes, call RESETSD
        MOVLW   d'217'      ; If not, load timer again
        MOVWF   TMR0
        GOTO    TMODO       ; Return to TMODO
MOSTRAR MOVF    NUMD,W      ; Function to display. Move NUMD to W
        CALL    DISPLAY     ; Call DISPLAY table
        MOVWF   PORTD       ; Load PORTD with what DISPLAY returned
        MOVF    SELDISP,W   ; Load W with the selected display
        MOVWF   PORTC       ; Load PORTC with the selected display
        DECFSZ  CONT2,F     ; Decrement CONT2 until 0 and save in itself
        GOTO    VOLVER      ; Continue until 0, go to VOLVER
        CALL    DECCONT     ; If zero, call DECCONT
        GOTO    VOLVER      
	
ENVIAR  CALL    ADC         ; Function to display data serially. Call ADC, ACTCH, and TDATO
        CALL    ACTCH
        CALL    TDATO
        BSF     STATUS,RP0
        MOVLW   D'25'       ; Baud rate = 9600bps
        MOVWF   SPBRG       ; At 4MHz
        MOVLW   B'00100100' 
        MOVWF   TXSTA       ; Configure TXSTA for 8-bit transmission, enable transmission, asynchronous mode, high speed
        BCF     STATUS,RP0  ; Bank 0
        MOVLW   B'10000000'
        MOVWF   RCSTA       ; Enable serial ports
        BCF     STATUS,RP0
        MOVF    CHAR0,W     ; Load W with CHAR0
        MOVWF   TXREG       ; Move W to TXREG, which sends serial data
        BSF     STATUS,RP0  ; Bank 1
WTHERE  BTFSS   TXSTA,TRMT  ; Is TRMT empty?
        GOTO    WTHERE      ; No, check again
        CLRF    TXSTA       ; Clear TXSTA
        BCF     STATUS,RP0 
        BCF     STATUS,C    ; Set carry to 0
        RLF     ECONT,F     ; Rotate ECONT and save in itself
        BTFSC   ECONT,5     ; Check the state of the 5th bit of ECONT
        CALL    RESEC       ; If it's 1, call RESEC
        MOVLW   D'3'        ; If it's 0, load W with 3
        MOVWF   CONTCS      ; Load CONTCS with 3
        RETURN

DECCONT MOVLW   D'3'        ; Load W with 3
        MOVWF   CONT2       ; Load CONT2 with 3
        DECFSZ  CONT,F      ; Decrement CONT and save in itself until it's zero
        RETURN              ; If it's 1, return
        MOVLW   0X04        ; If it's 0, load W with 4
        MOVWF   CONT        ; Load CONT with 4
        RETURN
	
UNSEG   BCF     STATUS,Z    ; Set bit Z of STATUS to 0 for the next subtraction operation
        MOVLW   D'200'      ; Load W with 200
        MOVWF   CONT3       ; Load CONT3 with 200
        INCF    SEG,F       ; Increment SEG and save in itself
        MOVLW   D'60'       ; Load W with 60
        SUBWF   SEG,W       ; Subtract SEG from W
        BTFSC   STATUS,Z    ; Check if it results in zero
        CALL    RESSEG      ; If it's 0, call RESSEG
        RETURN              ; If it's not 0 yet, return

ACTCH   MOVF    UNIDAD,W    ; Move UNIDAD to W
        ADDLW   0X30        ; Add 0x30 to W
        MOVWF   CHARU       ; Move that value to CHARU (in ASCII)
        MOVF    DECENA,W    ; Move DECENA to W
        ADDLW   0X30        ; Add 0x30 to W
        MOVWF   CHARD       ; Move that value to CHARD (in ASCII)
        MOVF    CENTENA,W   ; Move CENTENA to W
        ADDLW   0X30        ; Add 0x30 to W
        MOVWF   CHARC       ; Move that value to CHARC (in ASCII)
        RETURN              ; Return

RESEC   MOVLW   0X01
        MOVWF   ECONT
        RETURN

TDATO   BTFSC   ECONT,0     ; Function to determine which character is sent serially
        CALL    ENVUNI      ; If ECONT,1 is 1, call ENVUNI (sends unit)
        BTFSC   ECONT,1
        CALL    ENVCOM      ; If ECONT,2 is 1, call ENVCOM (sends comma)
        BTFSC   ECONT,2
        CALL    ENVDEC      ; If ECONT,3 is 1, call ENVDEC (sends decade)
        BTFSC   ECONT,3
        CALL    ENVCEN      ; If ECONT,4 is 1, call ENVCEN (sends hundred)
        BTFSC   ECONT,4
        CALL    ENVCR       ; If ECONT,5 is 1, call ENVCR (sends carriage return)
        BTFSC   ECONT,5
        CALL    ENVSALT     ; If ECONT,6 is 1, call ENVSALT (sends line feed)
        RETURN
	
ENVUNI  MOVF    CHARC,W     ; Move CHARC to W
        MOVWF   CHAR0       ; Load CHAR0 with W
        RETURN

ENVCOM  MOVLW   0X2C        ; Move 0x2c (Comma) to W
        MOVWF   CHAR0       ; Load CHAR0 with W
        RETURN

ENVDEC  MOVF    CHARD,W     ; Move CHARD to W
        MOVWF   CHAR0       ; Load CHAR0 with W
        RETURN

ENVCEN  MOVF    CHARU,W     ; Move CHARU to W
        MOVWF   CHAR0       ; Load CHAR0 with W
        RETURN

ENVCR   MOVLW   0X0D        ; Move 0x0d (Carriage Return) to W
        MOVWF   CHAR0       ; Load CHAR0 with W
        RETURN

ENVSALT MOVLW   0X0A        ; Move 0x0a (Line Feed) to W
        MOVWF   CHAR0       ; Load CHAR0 with W
        RETURN

PUSHED  BTFSC   PORTB,4     ; Function for the keyboard
        CALL    TC1         ; If the 4th bit of PORTB is 1, call TC1
        BTFSC   PORTB,5
        CALL    TC2         ; If the 5th bit of PORTB is 1, call TC1
        BTFSC   PORTB,6
        CALL    TC3         ; If the 6th bit of PORTB is 1, call TC1
        RETURN

MODIFY  BCF     STATUS,Z    ; Set bit Z of STATUS to 0
        MOVF    AUX,W       ; Move the pressed key to W
        SUBLW   0X0A        ; Subtract 0x0A (10) from W
        BTFSC   STATUS,Z    ; Check if it results in 0
        GOTO    CHMOD       ; If it's 0, go to CHMOD
        MOVF    AUX,W       ; If it's 1, move the pressed key to W
        SUBLW   0X0B        ; Subtract 0x0B (11) from W
        BTFSC   STATUS,Z    ; Check if it results in 0
        GOTO    CHMOD       ; If it's 0, go to CHMOD
        MOVF    SUHORA,W    ; If it's 1, move SUHORA to W
        MOVWF   SDHORA      ; Load SDHORA with W
        MOVF    SDMIN,W     ; Move SDMIN to W
        MOVWF   SUHORA      ; Load SUHORA with W
        MOVF    SUMIN,W     ; Move SUMIN to W
        MOVWF   SDMIN       ; Load SDMIN
		MOVF	AUX,W
		MOVWF	SUMIN
SALT	RETURN

	
ADC     BCF     ADCON0,2        ; Select CH0
        BSF     ADCON0,GO_DONE ; Start conversion
BA1     BTFSC   ADCON0,GO_DONE ; Check if conversion is finished
        GOTO    BA1             ; If not, go to BA1
        MOVF    ADRESH,W        ; Move ADRESH to W
        MOVWF   AUX3            ; Load AUX3 with W
        BCF     AUX3,2          ; Clear bits 0 and 2 of AUX3 for a range of values from 0 to 250 from the ADC
        BCF     AUX3,0          ; To have an exact increment of 0.02V
        BSF     ADCON0,2        ; Select CH1
        BSF     ADCON0,GO_DONE ; Start conversion
BA      BTFSC   ADCON0,GO_DONE ; Check if conversion is finished
        GOTO    BA              ; If not, go to BA
        MOVF    ADRESH,W        ; Move ADRESH to W
        MOVWF   AUX2            ; Load AUX2 with W
        BCF     AUX2,2          ; Clear bits 0 and 2 of AUX3 for a range of values from 0 to 250 from the ADC
        BCF     AUX2,0          ; To have an exact increment of 0.02A
        MOVF    AUX2,W          ; Move AUX2 to W
        CLRF    UNIDAD          ; Clear UNIDAD
        CLRF    DECENA          ; Clear DECENA
        CLRF    CENTENA         ; Clear CENTENA
        BTFSC   MODO,3         ; Check if we're in Mode 3
        MOVF    AUX3,W          ; If yes, move AUX3 to W
        BTFSC   MODO,2         ; If not, check if we're in Mode 2
        MOVF    AUX2,W          ; If yes, move AUX2 to W
        MOVWF   MCONT           ; Load MCONT with W
ACTN    MOVLW   0X02            ; Load W with 0x02
        ADDWF   UNIDAD,F        ; Add UNIDAD to W and store in UNIDAD
        BCF     STATUS,Z        ; Set Z flag to 0
        MOVF    UNIDAD,W        ; Move UNIDAD to W
        SUBLW   0X0A            ; Subtract 0x0A
        BTFSC   STATUS,Z        ; Check if the subtraction resulted in 0
        CALL    SOBREUN         ; If yes, call SOBREUN
        BCF     STATUS,Z        ; If not, set Z flag to 0
        DECFSZ  MCONT,F         ; Decrement MCONT until it's 0
        GOTO    ACTN            ; If not 0, go to ACTN
        MOVF    UNIDAD,W        ; Move UNIDAD to W if it's 0
        MOVWF   NUM1            ; Load NUM1 with W
        MOVF    DECENA,W        ; Move DECENA to W
        MOVWF   NUM2            ; Load NUM2 with W
        MOVF    CENTENA,W       ; Move CENTENA to W
        MOVWF   NUM3            ; Load NUM3 with W, all these are to shift the data to the left
        RETURN
	
SOBREUN     CLRF    UNIDAD      ; Clear UNIDAD
            INCF    DECENA,F    ; Increment DECENA by one and store it back
            BCF     STATUS,Z    ; Set Z flag to 0
            MOVF    DECENA,W    ; Move DECENA to W
            SUBLW   0X0A        ; Subtract 0x0A to prevent it from exceeding 9
            BTFSC   STATUS,Z    ; Check if the subtraction resulted in 0
            CALL    SODEC       ; If it resulted in 0, call SODEC
            RETURN              ; If not, return
            
SODEC       BCF     STATUS,Z    ; Set Z flag to 0
            CLRF    DECENA      ; Clear DECENA
            INCF    CENTENA,F   ; Increment CENTENA by one and store it back
            RETURN              ; Return
            
RESETSD     MOVLW   0X01        ; Reset. Load W with the literal 0X01
            MOVWF   SELDISP     ; Load SELDISP with W
            RETURN              ; Return
            
CHMOD       CALL    HSET        ; Call HSET
            BCF     STATUS,C    ; Set carry to 0
            RLF     MODO,F      ; Rotate mode and store it back
            BTFSC   MODO,4      ; Check if bit 4 of MODO is 1, indicating passing available modes
            CALL    RESMODO     ; If yes, call RESMODO
            INCF    CMOD,F      ; If not, increment CMOD and store it back
            MOVLW   0X04        ; Load W with the literal 0X04
            SUBWF   CMOD,W      ; Subtract CMOD from W
            BTFSC   STATUS,Z    ; Check if the subtraction resulted in 0
            CALL    RESMODO     ; If it resulted in 0, call RESMODO
            GOTO    SALT        ; If not, go to SALT
            
RESMODO     MOVLW   0X01        ; Reset. Load W with the literal 0X01
            MOVWF   MODO        ; Load MODO with W
            CLRF    CMOD        ; Clear CMOD
            RETURN              ; Return
            
HSET        MOVF    SUMIN,W     ; Move SUMIN to W
            MOVWF   UMIN        ; Load UMIN with W
            MOVF    SDMIN,W     ; Move SDMIN to W
            MOVWF   DMIN        ; Load DMIN with W
            MOVF    SUHORA,W    ; Move SUHORA to W
            MOVWF   UHORA       ; Load UHORA with W
            MOVF    SDHORA,W    ; Move SDHORA to W
            MOVWF   DHORA       ; Load DHORA with W
            RETURN              ; Return
	
RESSEG		BCF	STATUS,Z		;   Clear Z bit to 0
			CLRF	SEG			;   Clear SEG
			INCF	UMIN,F		;   Increment UMIN by one and store it in itself
			MOVLW	0X0A		;   Load W with the literal 0X0A (10, so it doesn't exceed 9)
			SUBWF	UMIN,W		;   Subtract from UMIN
			BTFSC	STATUS,Z	;   Check if the subtraction resulted in 0
			CALL	RESUMIN		;   If it's 0, call RESUMIN
			RETURN				;   If it's not 0, return
	
RESUMIN		CLRF UMIN			;   Clear UMIN
			INCF	DMIN,F		;   Increment DMIN by one and store it in itself
			MOVLW	0X06		;   Load W with the literal 0X06 (6, so it doesn't exceed 5)
			SUBWF	DMIN,W		;   Subtract from DMIN
			BTFSC	STATUS,Z	;   Check if the subtraction resulted in 0
			CALL	RESDMIN		;   If it's 0, call RESDMIN
			RETURN				;   If it's not 0, return
	
RESDMIN		CLRF	DMIN		;   Clear DMIN
			INCF	UHORA,F		;   Increment UHORA by one and store it in itself
			MOVLW	0X0A		;   Load W with the literal 0X0A (10, so it doesn't exceed 9)
			SUBWF	UHORA,W		;   Subtract from UHORA
			BTFSC	STATUS,Z	;   Check if the subtraction resulted in 0
			CALL	RESUH		;   If it's 0, call RESUH
			RETURN				;   If it's not 0, return
	
RESUH		CLRF	UHORA		;   Clear UHORA
			INCF	DHORA		;   Increment DHORA by one and store it in itself
			MOVLW	0X06		;   Load W with the literal 0X06 (6, so it doesn't exceed 5)
			SUBWF	DHORA,W		;   Subtract from DHORA
			BTFSC	STATUS,Z	;   Check if the subtraction resulted in 0
			CALL	RESDH		;   If it's 0, call RESDH
			RETURN				;   If it's not 0, return
	
RESDH		CLRF	DHORA		;   Clear DHORA
			RETURN			
		
    		END